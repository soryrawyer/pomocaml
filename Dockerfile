FROM debian:bookworm-slim

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y sqlite3 libsqlite3-dev libev4 libpq5 libargon2-1

RUN mkdir /app
COPY db/pom.db /app/pom.db
COPY _build/default/bin/main.exe /app/main.exe

ENTRYPOINT DB_STR=sqlite3:/app/pom.db /app/main.exe
