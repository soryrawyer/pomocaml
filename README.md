# pomocaml

track completed [pomodoro](https://en.wikipedia.org/wiki/Pomodoro_Technique) sessions

## getting set up

1. create a sqlite database and initialize it by running the commands in [db/schema.sql]
2. install the dependencies listed in `pomocaml.opam`
3. run the server with `DB_STR=sqlite3:/path/to/your/pom.db dune exec -- pomocaml`

the server code is in [bin/main.ml](./bin/main.ml). static assets are defined in [bin/static](./bin/static) and are bundled into the final binary using [crunch](https://github.com/mirage/ocaml-crunch).

## deploying

the build process currently happens outside of the Dockerfile and consists of these steps (also it assumes: you have an account with [fly.io](https://fly.io/); you've created a database locally at `./db/pom.db`):
1. build the executable with `dune build`
2. `fly deploy` (or `fly launch` if deploying into a new fly app)
