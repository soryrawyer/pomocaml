-- fyi, this can be used to get a readable string from created_at:
-- select datetime(created_at, 'unixepoch') from pomodoros;
drop table if exists pomodoros;
create table pomodoros (
  id integer primary key autoincrement,
  title text,
  description text,
  created_at int
);

drop table if exists tags;
create table tags (
  id integer primary key autoincrement,
  name text not null unique
);

drop table if exists pom_tags;
create table pom_tags (
  id integer primary key autoincrement,
  pom_id integer,
  tag_id integer,
  foreign key (pom_id) references pomodoros(id),
  foreign key (tag_id) references tags(id)
);

drop table if exists users;
create table users (
  id integer primary key autoincrement,
  name text not null unique,
  hashed_password text not null
);

drop table if exists dream_session;
CREATE TABLE dream_session (
  id TEXT PRIMARY KEY,
  label TEXT NOT NULL,
  expires_at REAL NOT NULL,
  payload TEXT NOT NULL
);
