module Hash = Argon2.ID

let hash password =
  let hash_len = 16 in
  let salt_len = 16 in
  let t_cost = 2 in
  let m_cost = 50 * 1024 in
  let parallelism = 4 in
  let salt = Dream.random salt_len in
  let encoded_len =
    Argon2.encoded_len ~t_cost ~m_cost ~parallelism ~salt_len ~hash_len ~kind:ID
  in
  let result =
    Hash.hash_encoded ~t_cost ~m_cost ~parallelism ~pwd:password ~salt ~hash_len
      ~encoded_len
  in
  match result with
  | Ok encoded -> Hash.encoded_to_string encoded
  | Error err ->
      failwith
        (Printf.sprintf "failed to hash password: %s"
           (Argon2.ErrorCodes.message err))

let verify hash password =
  match Argon2.verify ~encoded:hash ~pwd:password ~kind:ID with
  | Ok matches -> Ok matches
  | Error err ->
      Error
        (Printf.sprintf "error during verification: %s"
           (Argon2.ErrorCodes.message err))
