val get : Dream.request -> Dream.response Lwt.t
val post : Dream.request -> Dream.response Lwt.t

(* these next two can be used as get and post handlers, respectively, *)
(* if you want to allow folks to sign up *)
val signup_form : Dream.request -> Dream.response Lwt.t
val signup : Dream.request -> Dream.response Lwt.t
