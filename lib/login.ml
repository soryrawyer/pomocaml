open Tyxml.Html

let auth_form url token =
  form
    ~a:[ a_action url; a_id "auth_form"; a_method `Post ]
    [
      input ~a:[ a_name "dream.csrf"; a_input_type `Hidden; a_value token ] ();
      ul
        [
          li
            [
              label ~a:[ a_label_for "username" ] [ txt "user name:" ];
              input
                ~a:[ a_input_type `Text; a_name "username"; a_id "username" ]
                ();
            ];
          li
            [
              label ~a:[ a_label_for "password" ] [ txt "pass word:" ];
              input
                ~a:
                  [ a_input_type `Password; a_name "password"; a_id "password" ]
                ();
            ];
          li [ button ~a:[ a_button_type `Submit ] [ txt "log in" ] ];
        ];
    ]

let css = link ~rel:[ `Stylesheet ] ~href:"/static/base.css" ()
let common_title = title (txt "pomocaml")
let common_wrapper inner = html (head common_title [ css ]) (body inner)
let string_of_html html = Format.asprintf "%a" (Tyxml.Html.pp ()) html

let get request =
  let token = Dream.csrf_token request in
  Dream.html (string_of_html (common_wrapper [ auth_form "/login" token ]))

let post request =
  match%lwt Dream.form request with
  | `Ok [ ("password", password); ("username", username) ] -> (
      Dream.log "got login request for %s" username;
      match%lwt Dream.sql request (Models.User.get username) with
      | Some user -> (
          match Auth.verify user.password password with
          | Ok true ->
              let%lwt () =
                Dream.set_session_field request "username" username
              in
              Dream.redirect request "/"
          | _ -> Dream.empty `Unauthorized)
      | None ->
          Dream.log "no user found with the name %s" username;
          Dream.empty `Unauthorized)
  | _ -> Dream.empty `Bad_Request

let signup_form request =
  let token = Dream.csrf_token request in
  Dream.html (string_of_html (common_wrapper [ auth_form "/signup" token ]))

let signup request =
  match%lwt Dream.form request with
  | `Ok [ ("password", password); ("username", username) ] ->
      let hashed_pw = Auth.hash password in
      let _ =
        match%lwt Dream.sql request (Models.User.write username hashed_pw) with
        | Ok _ ->
            Dream.log "sick";
            Lwt.return_unit
        | Error _ ->
            Dream.log "oh no!";
            Lwt.return_unit
      in
      Dream.redirect request "/login"
  | _ -> Dream.empty `Bad_Request
