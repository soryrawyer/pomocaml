module type DB = Caqti_lwt.CONNECTION

type t = { id : int; name : string }

val write : string -> (module DB) -> (int option, Db_ops.db_error) result Lwt.t
val get_list : (module DB) -> t list Lwt.t
val option_of_tag : t -> [> Html_types.selectoption ] Tyxml_html.elt
