open Lwt.Infix
open Tyxml.Html

module type DB = Caqti_lwt.CONNECTION

module R = Caqti_request.Infix
module T = Caqti_type

type t = {
  id : int;
  title : string;
  description : string;
  tag_names : string;
  created_at : string;
}

let get_one =
  let query =
    R.(T.string ->! T.(tup4 int string string T.(tup2 string string)))
      {|
      select
        p.id,
        p.title,
        p.description,
        strftime('%Y-%m-%d %H:%M', p.created_at, 'unixepoch', 'localtime') as created_at,
        coalesce(group_concat(t.name, ', '), 'no tags') as tag_names
      from
        pomodoros p
        left join pom_tags pt
          on p.id = pt.pom_id
        left join tags t
          on pt.tag_id = t.id
      where p.id = $1
      group by 1, 2, 3, 4
      order by p.created_at desc
  |}
  in
  fun pom_id (module Db : DB) ->
    Db.find_opt query pom_id >>= Caqti_lwt.or_fail >>= fun details_or_error ->
    Lwt.return
      (Option.map
         (fun (id, title, description, (created_at, tag_names)) ->
           { id; title; description; tag_names; created_at })
         details_or_error)

let get_list =
  let query =
    R.(T.unit ->* T.(tup4 int string string T.(tup2 string string)))
      {|
      select
        p.id,
        p.title,
        p.description,
        strftime('%Y-%m-%d %H:%M', p.created_at, 'unixepoch', 'localtime') as created_at,
        coalesce(group_concat(t.name, ', '), 'no tags') as tag_names
      from
        pomodoros p
        left join pom_tags pt
          on p.id = pt.pom_id
        left join tags t
          on pt.tag_id = t.id
      group by 1, 2, 3, 4
      order by p.created_at desc
|}
  in
  fun (module Db : DB) ->
    Db.collect_list query () >>= Caqti_lwt.or_fail >>= fun rows ->
    Lwt.return
      (List.map
         (fun (id, title, description, (created_at, tag_names)) ->
           { id; title; description; tag_names; created_at })
         rows)

let write =
  let query =
    R.(T.(tup2 string string) ->. T.unit)
      {|
      insert into pomodoros (title, description, created_at)
      values ($1, $2, strftime('%s'))
    |}
  in
  fun title description (module Db : DB) ->
    Db.exec query (title, description)
    >>= Db_ops.early_error (fun _ ->
            match Db_ops.get_last_rowid Db.driver_connection with
            | Ok (Some row_id) -> Ok row_id
            | Ok None -> Error Db_ops.CaqtiError
            | Error err -> Error (Db_ops.DriverError err))

let li_of_pom pom =
  li
    ~a:[ a_id (string_of_int pom.id) ]
    [
      div [ label [ b [ txt "title:" ] ]; p [ txt pom.title ] ];
      div [ label [ b [ txt "description:" ] ]; p [ txt pom.description ] ];
      div [ label [ b [ txt "tags:" ] ]; p [ txt pom.tag_names ] ];
      div [ label [ b [ txt "created at:" ] ]; p [ txt pom.created_at ] ];
      div
        [
          a ~a:[ a_href (Printf.sprintf "/poms/%d/edit" pom.id) ] [ txt "edit" ];
        ];
    ]

let update =
  let query =
    R.(T.(tup3 int string string) ->. T.unit)
      {|
        update pomodoros
        set title = $2, description = $3
        where id = $1
      |}
  in
  fun pom_id title description (module Db : DB) ->
    Db.exec query (pom_id, title, description) >>= Caqti_lwt.or_fail
