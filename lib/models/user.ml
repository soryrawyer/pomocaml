module type DB = Caqti_lwt.CONNECTION

module T = Caqti_type
module R = Caqti_request.Infix
open Lwt.Infix

type t = { id : int; username : string; password : string }

let write =
  let query =
    R.(T.(tup2 string string) ->. T.unit)
      "insert into users (name, hashed_password) values ($1, $2)"
  in
  fun username password (module Db : DB) ->
    Db.exec query (username, password)
    >>= Db_ops.early_error (fun _ ->
            match Db_ops.get_last_rowid Db.driver_connection with
            | Ok row_id_opt -> Ok row_id_opt
            | Error err -> Error (Db_ops.DriverError err))

let get =
  let query =
    R.(T.string ->! T.(tup3 int string string))
      "select id, name, hashed_password from users where name = $1"
  in
  fun username (module Db : DB) ->
    Db.find_opt query username >>= Caqti_lwt.or_fail >>= fun details_or_error ->
    Lwt.return
      (Option.map
         (fun (id, username, password) -> { id; username; password })
         details_or_error)
