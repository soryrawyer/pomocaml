module type DB = Caqti_lwt.CONNECTION

type t = {
  id : int;
  title : string;
  description : string;
  tag_names : string;
  created_at : string;
}

val write :
  string -> string -> (module DB) -> (int, Db_ops.db_error) result Lwt.t

val get_one : string -> (module DB) -> t option Lwt.t
val get_list : (module DB) -> t list Lwt.t
val li_of_pom : t -> [> Html_types.li ] Tyxml_html.elt
val update : int -> string -> string -> (module DB) -> unit Lwt.t
