(* TODO figure out how to generate documentation for these tags *)
type driver_error = DriverUnavailable | IncompatibleDriver
type db_error = CaqtiError | DriverError of driver_error

val get_last_rowid :
  Caqti_connection_sig.driver_connection option ->
  (int option, driver_error) result

val early_error :
  (unit -> ('b, db_error) result) ->
  ('c, [> Caqti_error.call_or_retrieve ]) result ->
  ('b, db_error) result Lwt.t
