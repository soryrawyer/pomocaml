module type DB = Caqti_lwt.CONNECTION

type t = { id : int; username : string; password : string }

val write :
  string -> string -> (module DB) -> (int option, Db_ops.db_error) result Lwt.t

val get : string -> (module DB) -> t option Lwt.t
