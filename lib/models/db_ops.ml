type driver_error = DriverUnavailable | IncompatibleDriver
type db_error = CaqtiError | DriverError of driver_error

let early_error f = function
  | Error _ -> Lwt.return_error CaqtiError
  | Ok _ -> (
      match f () with
      | Ok value -> Lwt.return_ok value
      | Error err -> Lwt.return_error err)

let get_last_rowid driver_connection =
  let get_rowid_opt conn =
    try Some (Sqlite3.last_insert_rowid conn |> Int64.to_int)
    with Sqlite3.SqliteError _ -> None
  in
  match driver_connection with
  | None -> Error DriverUnavailable
  | Some (Caqti_driver_sqlite3.Driver_connection db) -> Ok (get_rowid_opt db)
  | Some _ -> Error IncompatibleDriver
