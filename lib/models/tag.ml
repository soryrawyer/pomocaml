module type DB = Caqti_lwt.CONNECTION

module T = Caqti_type
module R = Caqti_request.Infix
open Lwt.Infix
open Tyxml.Html

type t = { id : int; name : string }

let write =
  let query =
    R.(T.string ->. T.unit) "insert or ignore into tags (name) values ($1)"
  in
  fun name (module Db : DB) ->
    Db.exec query name
    >>= Db_ops.early_error (fun _ ->
            match Db_ops.get_last_rowid Db.driver_connection with
            | Ok row_id_opt -> Ok row_id_opt
            | Error err -> Error (Db_ops.DriverError err))

let get_list =
  let query =
    R.(T.unit ->* T.(tup2 int string))
      "select id, name from tags order by name asc"
  in
  fun (module Db : DB) ->
    Db.collect_list query () >>= Caqti_lwt.or_fail >>= fun rows ->
    Lwt.return (List.map (fun (id, name) -> { id; name }) rows)

let option_of_tag tag =
  option ~a:[ a_value tag.name; a_id (string_of_int tag.id) ] (txt tag.name)
