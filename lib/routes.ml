let routes =
  [
    Dream.get "/" Views.Home.get;
    Dream.get "/login" Login.get;
    Dream.post "/login" Login.post;
    Dream.post "/logout" (fun request ->
        let%lwt () = Dream.invalidate_session request in
        Dream.redirect request "/login");
    Dream.get "/poms/:pom_id" Views.Poms.get;
    Dream.get "/poms/:pom_id/edit" Views.Poms.edit;
    Dream.post "/poms/:pom_id" Views.Poms.update;
    Dream.post "/poms" Views.Poms.post;
  ]
