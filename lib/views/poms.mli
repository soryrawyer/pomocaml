(* val get : Dream.request -> Dream.response Lwt.t *)
val post : Dream.request -> Dream.response Lwt.t
val get : Dream.request -> Dream.response Lwt.t
val edit : Dream.request -> Dream.response Lwt.t
val update : Dream.request -> Dream.response Lwt.t

val pom_form :
  ?title:string ->
  ?description:string ->
  string ->
  string ->
  Models.Tag.t list ->
  [> Html_types.form ] Tyxml_html.elt
