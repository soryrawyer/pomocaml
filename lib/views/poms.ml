open Tyxml.Html
module R = Caqti_request.Infix
module T = Caqti_type
module Pom = Models.Pom
module Tag = Models.Tag
module SS = Set.Make (String)

let form_title title =
  li
    [
      label ~a:[ a_label_for "pom_title" ] [ txt "title:" ];
      input
        ~a:
          [
            a_input_type `Text;
            a_name "pom_title";
            a_id "pom_title";
            a_value title;
          ]
        ();
    ]

let form_description text =
  li
    [
      label ~a:[ a_label_for "pom_description" ] [ txt "pom activities:" ];
      textarea
        ~a:
          [
            a_name "pom_description";
            a_id "pom_description";
            a_cols 80;
            a_rows 10;
          ]
        (txt text);
    ]

let form_tag_select tags =
  li
    [
      label ~a:[ a_label_for "tag_select" ] [ txt "tags" ];
      select
        ~a:[ a_name "tag_select"; a_id "tag_select"; a_multiple () ]
        (List.map Tag.option_of_tag tags);
      label ~a:[ a_label_for "new_tag" ] [ txt "add a new tag" ];
      input ~a:[ a_input_type `Text; a_name "new_tag"; a_id "new_tag" ] ();
    ]

(* TODO *)
(* - accept an endpoint for the action *)
(* - accept default values for: title; description; tags *)
(* - any existing tags should be pre-selected *)
let pom_form ?(title = "") ?(description = "") action_url token tags =
  form
    ~a:[ a_action action_url; a_id "new_pom_form"; a_method `Post ]
    [
      input ~a:[ a_name "dream.csrf"; a_input_type `Hidden; a_value token ] ();
      ul
        [
          form_title title;
          form_description description;
          form_tag_select tags;
          li [ button ~a:[ a_button_type `Submit ] [ txt "add pom" ] ];
        ];
    ]

type pom_form = {
  new_tags : string list option;
  title : string;
  description : string;
  tags : string list;
}

exception InvalidForm

let parse_form_fields fields =
  let get_one field_name =
    match List.filter (fun (field, _) -> field = field_name) fields with
    | [] -> raise InvalidForm
    | [ (_, value) ] -> value
    | _ -> raise InvalidForm
  in
  let new_tags =
    match List.filter (fun (field, _) -> field = "new_tag") fields with
    | [] -> None
    | [ (_, tags) ] ->
        if tags = "" then None else Some (String.split_on_char ',' tags)
    | _ -> None
  in
  let description = get_one "pom_description" in
  let title = get_one "pom_title" in
  let tags =
    List.filter (fun (field, _) -> field = "tag_select") fields
    |> List.map (fun (_, x) -> x)
  in
  { new_tags; description; title; tags }

let rec add_tags request = function
  | [] -> Lwt.return ()
  | hd :: tl ->
      let%lwt _ = Dream.sql request (Tag.write hd) in
      add_tags request tl

let add_pom_tag =
  let query =
    R.(T.(tup2 int int) ->. T.unit)
      "insert into pom_tags (pom_id, tag_id) values ($1, $2)"
  in
  fun pom_id tag_id (module Db : Caqti_lwt.CONNECTION) ->
    let%lwt unit_or_error = Db.exec query (pom_id, tag_id) in
    Caqti_lwt.or_fail unit_or_error

let create_pom_tags request tags pom_id =
  let rec aux = function
    | [] -> Lwt.return_unit
    | hd :: tl ->
        let%lwt () = Dream.sql request (add_pom_tag pom_id hd) in
        aux tl
  in
  aux tags

let handle_pom_form request fields =
  try
    let form_fields = parse_form_fields fields in
    let%lwt row_id_res =
      Dream.sql request (Pom.write form_fields.title form_fields.description)
    in
    let pom_id =
      match row_id_res with Error _ -> raise InvalidForm | Ok row_id -> row_id
    in
    let%lwt new_tags =
      match form_fields.new_tags with
      | None -> Lwt.return []
      | Some new_tags ->
          let%lwt _ = add_tags request new_tags in
          Lwt.return new_tags
    in
    let%lwt all_tags = Dream.sql request Tag.get_list in
    let pom_tag_names =
      List.fold_right SS.add
        (List.concat [ form_fields.tags; new_tags ])
        SS.empty
    in
    let is_pom_tag (tag : Tag.t) = SS.mem tag.name pom_tag_names in
    let pom_tag_ids =
      List.filter is_pom_tag all_tags |> List.map (fun (t : Tag.t) -> t.id)
    in
    let%lwt _ = create_pom_tags request pom_tag_ids pom_id in
    Dream.redirect request "/"
  with InvalidForm -> Dream.empty `Bad_Request

let post request =
  match%lwt Dream.form request with
  | `Ok fields -> handle_pom_form request fields
  | _ ->
      Dream.log "bad request, huh";
      Dream.empty `Bad_Request

let css = link ~rel:[ `Stylesheet ] ~href:"/static/base.css" ()
let common_title = title (txt "pomocaml")
let common_wrapper inner = html (head common_title [ css ]) (body inner)
let string_of_html html = Format.asprintf "%a" (Tyxml.Html.pp ()) html

let handle_pom request html_of_pom =
  let pom_id = Dream.param request "pom_id" in
  match%lwt Dream.sql request (Pom.get_one pom_id) with
  | Some pom ->
      let%lwt result = html_of_pom pom in
      Dream.html result
  | None -> Dream.empty `Bad_Request

let get request =
  let pom_html (pom : Pom.t) = [ txt pom.title; txt pom.description ] in
  let html_of_pom pom =
    pom_html pom |> common_wrapper |> string_of_html |> Lwt.return
  in
  handle_pom request html_of_pom

let edit request =
  let html_of_pom (pom : Pom.t) =
    let pom_edit_url = Printf.sprintf "/poms/%d" pom.id in
    let token = Dream.csrf_token request in
    let%lwt tags = Dream.sql request Tag.get_list in
    let edit_form =
      pom_form ~title:pom.title ~description:pom.description pom_edit_url token
        tags
    in
    Lwt.return
      (common_wrapper [ section [ h3 [ txt "update pom" ]; edit_form ] ]
      |> string_of_html)
  in
  handle_pom request html_of_pom

(* get pom id
   get data from fields
   update pom
   create new tags*)
let update request =
  let pom_id = int_of_string (Dream.param request "pom_id") in
  Dream.log "updating pom %d" pom_id;
  let process_pom (fields : pom_form) =
    (* update pom *)
    (* add any new tags *)
    (* add any new pom_tags records *)
    let%lwt _ =
      Dream.sql request (Pom.update pom_id fields.title fields.description)
    in
    let%lwt new_tags =
      match fields.new_tags with
      | None -> Lwt.return []
      | Some new_tags ->
          let%lwt _ = add_tags request new_tags in
          Lwt.return new_tags
    in
    let%lwt all_tags = Dream.sql request Tag.get_list in
    let pom_tag_names =
      List.fold_right SS.add (List.concat [ fields.tags; new_tags ]) SS.empty
    in
    let is_pom_tag (tag : Tag.t) = SS.mem tag.name pom_tag_names in
    let pom_tag_ids =
      List.filter is_pom_tag all_tags |> List.map (fun (t : Tag.t) -> t.id)
    in
    let%lwt _ = create_pom_tags request pom_tag_ids pom_id in
    Lwt.return ()
  in
  match%lwt Dream.form request with
  | `Ok fields -> (
      try
        let form_fields = parse_form_fields fields in
        let%lwt () = process_pom form_fields in
        Dream.redirect request "/"
      with InvalidForm -> Dream.empty `Bad_Request)
  | _ -> Dream.empty `Bad_Request
