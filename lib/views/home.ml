module Pom = Models.Pom
module Tag = Models.Tag
open Tyxml.Html

let css = link ~rel:[ `Stylesheet ] ~href:"/static/base.css" ()
let common_title = title (txt "pomocaml")
let common_wrapper inner = html (head common_title [ css ]) (body inner)
let string_of_html html = Format.asprintf "%a" (Tyxml.Html.pp ()) html

let display_poms request =
  let%lwt poms = Dream.sql request Pom.get_list in
  Lwt.return (ul ~a:[ a_id "pom-list" ] (List.map Pom.li_of_pom poms))

let home request username =
  let token = Dream.csrf_token request in
  let message = Printf.sprintf "%s's poms" username in
  let%lwt finished_poms = display_poms request in
  let%lwt tags = Dream.sql request Tag.get_list in
  let pom_form = Poms.pom_form "/poms" token tags in
  Lwt.return
    (string_of_html
       (common_wrapper
          [
            section [ h1 [ txt message ] ];
            section [ h3 [ txt "log a new pom" ]; pom_form ];
            section [ h3 [ txt "completed poms" ]; finished_poms ];
          ]))

let get request =
  match Dream.session_field request "username" with
  | Some username ->
      let%lwt resp = home request username in
      Dream.html resp
  | None -> Dream.redirect request "/login"
