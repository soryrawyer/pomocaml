(* ok ok ok ok ok I know this isn't great, but in my defense: *)
(* I don't know any better. this just checks to see if there's a *)
(* username field in the session and if not (and we're not already *)
(* headed to the login page) then we should redirect the pomodorian *)
(* to the login page *)
let auth_middleware inner_handler (request : Dream.request) =
  if Dream.target request = "/login" then inner_handler request
  else
    match Dream.session_field request "username" with
    | Some _ -> inner_handler request
    | None -> Dream.redirect request "/login"

let loader _root path _request =
  match Static.read path with
  | None -> Dream.empty `Not_Found
  | Some asset -> Dream.respond asset

(* use our cool new assets loader for static assets *)
(* and our pomocaml routes for the rest *)
let routes =
  Dream.get "/static/**" (Dream.static ~loader "") :: Pomocaml.Routes.routes

let start_server message =
  Dream.log "%s" message;
  let interface = try Unix.getenv "INTERFACE" with Not_found -> "localhost" in
  let db_conn_str = Unix.getenv "DB_STR" in
  Dream.run ~interface @@ Dream.logger @@ Dream.sql_pool db_conn_str
  @@ Dream.sql_sessions @@ auth_middleware @@ Dream.router routes

let server_command =
  Core.Command.basic ~summary:"pomocaml web server"
    (let%map_open.Core.Command message = anon ("message" %: string) in
     fun () -> start_server message)

let commands =
  Core.Command.group ~summary:"pomocaml: track pomodoro usage"
    [ ("server", server_command); ("create-user", Create_user.command) ]

let () = Command_unix.run ~version:"0.1.0" ~build_info:"RWO" commands
