let create_user username password =
  let db_conn_str = Unix.getenv "DB_STR" in
  let hashed_pw = Pomocaml.Auth.hash password in
  let start =
    match%lwt Caqti_lwt.connect (Uri.of_string db_conn_str) with
    | Error _ -> Lwt.return_unit
    | Ok conn -> (
        match%lwt Models.User.write username hashed_pw conn with
        | Ok _ ->
            Printf.printf "successfully created user %s\n" username;
            Lwt.return_unit
        | Error _ ->
            Printf.printf "uh oh!";
            Lwt.return_unit)
  in
  Lwt_main.run start

let command =
  Core.Command.basic ~summary:"create a new user"
    (let%map_open.Core.Command username = anon ("username" %: string)
     and password = anon ("password" %: string) in
     fun () -> create_user username password)
